<?php namespace Ortnit\Lib;
/**
 * Created by PhpStorm.
 * User: tobi
 * Date: 19.05.2016
 * Time: 16:42
 *
 */

use Ortnit\Lib\System\Path;

class ArkTools
{
    /**
     * https://github.com/project-umbrella/arkit.py/blob/master/arkit.py
     *
     * @param $source
     * @param $destination
     * @throws \Exception
     *
     */
    public static function decompress($source, $destination) {
        if(is_file($source)) {
            $fp = fopen($source, 'rb');
            $signatureVersion = unpack('q', fread($fp, 8))[1];
            $sizeUnpackedChunk = unpack('q', fread($fp, 8))[1];
            $sizePacked = unpack('q', fread($fp, 8))[1];
            //fseek($fp, 8, SEEK_CUR);
            $sizeUnpacked = unpack('q', fread($fp, 8))[1];
            //var_dump($signatureVersion);
            if($signatureVersion == 2653586369) {
                $chunks = [];
                $size = 0;
                while ($size < $sizeUnpacked) {
                    $rawCompressed = fread($fp, 8);
                    $compressed = unpack('q', $rawCompressed)[1];
                    $rawUncompressed = fread($fp, 8);
                    $uncompressed = unpack('q', $rawUncompressed)[1];
                    $chunks[] = [
                        'compressed' => $compressed,
                        'uncompressed' => $uncompressed
                    ];
                    $size += $uncompressed;
                    printf("%d: %d/%d (%d/%d) %d\n", count($chunks), $size, $sizeUnpacked, $compressed, $uncompressed, $sizePacked);
                }

                if($size != $sizeUnpacked) {
                    throw new \Exception(sprintf('Header-Index mismatch. Header indicates it should only have %s bytes when uncompressed but the index indicates %s bytes.', $sizeUnpacked, $size));
                }

                $data = '';
                $chunkCount = 0;
                foreach($chunks as $chunk) {
                    $compressedData = fread($fp, $chunk['compressed']);
                    $uncompressedData = zlib_decode($compressedData);

                    if(strlen($uncompressedData) == $chunk['uncompressed']) {
                        $data .= $uncompressedData;
                        $chunkCount++;

                        if($chunkCount != count($chunks) and $sizeUnpackedChunk != strlen($uncompressedData)) {
                            throw new \Exception(sprintf('Index contains more than one partial chunk: was %d when the full chunk size is %d, chunk %d/%d', strlen($uncompressedData), $sizeUnpackedChunk, $chunkCount, strlen($chunks)));
                        }
                    } else {
                        throw new \Exception(sprintf('Uncompressed chunk size is not the same as in the index: was %d but should be %d', strlen($uncompressedData), $chunk['uncompressed']));
                    }
                }
            } else {
                throw new \Exception(sprintf('The signature and format version is incorrect. Signature was %s should be 2653586369.', $signatureVersion));
            }
            //var_dump($signatureVersion, $sizeUnpackedChunk, $sizePacked, $sizeUnpacked);
            fclose($fp);
            $fp = fopen($destination, 'w+b');
            fwrite($fp, $data);
            fclose($fp);
        } else {
            throw new \Exception(sprintf('File not found %s', $source));
        }
    }

    /**
     * create ark mod files
     *
     * @param $directory
     * @param $modId
     * @param $name
     * @return bool
     */
    public static function createModFile($directory, $modId, $name) {
        //var_dump($directory, $modId, $name);
        $modInfo = self::_getModInfo($directory, $modId);
        //var_dump($modInfo);
        $modMetaInfo = self::_getModMetaInfo($directory, $modId);
        //var_dump($modMetaInfo);

        $modFileContent = '';
        $modFileContent .= pack('V2', intval($modId), 0);
        $modFileContent .= self::_createBinaryString($name);
        $modFileContent .= self::_createBinaryString(Path::joinPath('../../../ShooterGame/Content/Mods/', $modId));
        $modFileContent .= pack('V', 1);
        $modFileContent .= self::_createBinaryString($modInfo['filename']);
        $modFileContent .= pack('V2', 4280483635, 2);
        if (!empty($modMetaInfo)) {
            $modFileContent .= pack('C', $modMetaInfo['modType']);
            $modFileContent .= $modMetaInfo['content'];
        } else {
            $modFileContent .= pack('CV', 0, 0);
        }
        //var_dump($modFileContent);
        $path = Path::joinPath($directory, $modId . '.mod');
        $fp = fopen($path, 'w+');
        fwrite($fp, $modFileContent);
        fclose($fp);
        return is_file($path);
    }

    protected static function _getModInfo($directory, $modId) {
        $result = [];
        $path = Path::joinPath($directory, $modId, 'mod.info');
        $fp = fopen($path, 'rb');
        $data = unpack('Vlength', fread($fp, 4));
        //var_dump($data);
        $result['name'] = '';
        if($data['length'] != 0) {
            $result['name'] = trim(fread($fp, $data['length']));
        }
        $data = unpack('Vlength', fread($fp, 4));
        //var_dump($data);
        $data = unpack('Vlength', fread($fp, 4));
        //var_dump($data);
        $result['filename'] = trim(fread($fp, $data['length']));
        fclose($fp);
        return $result;
    }

    protected static function _getModMetaInfo($directory, $modId) {
        $result = [];
        $path = Path::joinPath($directory, $modId, 'modmeta.info');
        if(is_file($path)) {
            $result['content'] = file_get_contents($path);
            $result['modType'] = ord($result['content']{20}) - 48; //string to int conversion
        }
        //var_dump($result);
        return $result;
    }

    protected static function _createBinaryString($string) {
        $result = '';
        $result .= pack('V', (strlen($string) + 1));
        $result .= $string;
        $result .= "\x00";
        return $result;
    }
}